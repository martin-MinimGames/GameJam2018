﻿
using UnityEngine;

public class Builder : MonoBehaviour
{
    void Start()
    {
        var landscape = new Landscape();

        landscape.BuildWorld();
    }
}