﻿
using System;
using UnityEngine;

public class UserInteraction : MonoBehaviour
{
    Renderer myRenderer;

    DateTime chargeStarted;

    bool broken;

    void Start()
    {
        myRenderer = GetComponent<Renderer>();
    }
    void OnMouseDown()
    {
        myRenderer.material.color = Color.yellow;
        chargeStarted = DateTime.Now;
        Invoke("Break", 1);
    }
    void OnMouseUp()
    {
        CancelInvoke("Break");
        if (!broken)
        {
            myRenderer.material.color = Color.green;
        }
    }

    public void Break()
    {
        Debug.LogWarning("Break");
        myRenderer.material.color = Color.red;
        Time.timeScale = 0;
        broken = true;
    }

}
